# Hi 👋 I'm Adith A Danthi
### Google Certified Android Developer | Web developer

<a href="https://github.com/adith-a-danthi" target="blank">
    <img align="center" src="https://img.shields.io/badge/GitHub-black?style=for-the-badge&logo=GitHub" alt="adith_danthi"/>
</a>
<a href="https://linkedin.com/in/adith-a-danthi" target="blank">
    <img align="center" src="https://img.shields.io/badge/LinkedIn-black?style=for-the-badge&logo=linkedin&logoColor=blue" alt="adith_danthi"/>
</a>
<a href="https://www.hackerrank.com/adith_danthi" target="blank">
    <img align="center" src="https://img.shields.io/badge/Twitter-black?style=for-the-badge&logo=twitter" alt="adith_danthi"/>
</a>


- 🌱 I’m currently learning `Vue.js` & `React.js`

- ⚡ Fun fact **I'm a Techie and a Swimmer**


### Skills
#### Frontend Technologies
![HTML](https://img.shields.io/badge/HTML-black?style=for-the-badge&logo=HTML5)
![CSS](https://img.shields.io/badge/CSS-black?style=for-the-badge&logo=CSS3&logoColor=1572B6)
![JavaScript](https://img.shields.io/badge/JavaScript-black?style=for-the-badge&logo=javascript)
![SASS](https://img.shields.io/badge/SASS-black?style=for-the-badge&logo=SASS)
![Vue.js](https://img.shields.io/badge/Vue.js-black?style=for-the-badge&logo=Vue.js)
![Nuxt.js](https://img.shields.io/badge/Nuxt.js-black?style=for-the-badge&logo=Nuxt.js)

#### Mobile Development
![Android](https://img.shields.io/badge/Android-black?style=for-the-badge&logo=Android)
![Kotlin](https://img.shields.io/badge/Kotlin-black?style=for-the-badge&logo=Kotlin)
![Java](https://img.shields.io/badge/Java-black?style=for-the-badge&logo=java)

#### Backend Technologies
![Go](https://img.shields.io/badge/Go-black?style=for-the-badge&logo=Go)
![Python](https://img.shields.io/badge/Python-black?style=for-the-badge&logo=Python)
![Flask](https://img.shields.io/badge/Flask-black?style=for-the-badge&logo=Flask)
![MongoDB](https://img.shields.io/badge/MongoDB-black?style=for-the-badge&logo=MongoDB)
![MySQL](https://img.shields.io/badge/MySQL-black?style=for-the-badge&logo=MySQL&logoColor=1E8CBE)
![Postgres](https://img.shields.io/badge/Postgresql-black?style=for-the-badge&logo=Postgresql)

#### Other Technologies
![Git](https://img.shields.io/badge/Git-black?style=for-the-badge&logo=Git)
![Firebase](https://img.shields.io/badge/Firebase-black?style=for-the-badge&logo=Firebase)
